#include "openglwindow.hpp"

#include <fmt/core.h>
#include <imgui.h>
#include <tiny_obj_loader.h>

#include <cppitertools/itertools.hpp>
#include <glm/gtx/fast_trigonometry.hpp>
#include <glm/gtx/hash.hpp>
#include <unordered_map>


void OpenGLWindow::handleEvent(SDL_Event& ev) {
  if (ev.type == SDL_KEYDOWN) {
    if (ev.key.keysym.sym == SDLK_UP || ev.key.keysym.sym == SDLK_w)
      m_dollySpeed = 1.0f;
    if (ev.key.keysym.sym == SDLK_DOWN || ev.key.keysym.sym == SDLK_s)
      m_dollySpeed = -1.0f;
    if (ev.key.keysym.sym == SDLK_LEFT || ev.key.keysym.sym == SDLK_a)
      m_panSpeed = -1.0f;
    if (ev.key.keysym.sym == SDLK_RIGHT || ev.key.keysym.sym == SDLK_d)
      m_panSpeed = 1.0f;
    if (ev.key.keysym.sym == SDLK_q) m_truckSpeed = -1.0f;
    if (ev.key.keysym.sym == SDLK_e) m_truckSpeed = 1.0f;
  }
  if (ev.type == SDL_KEYUP) {
    if ((ev.key.keysym.sym == SDLK_UP || ev.key.keysym.sym == SDLK_w) &&
        m_dollySpeed > 0)
      m_dollySpeed = 0.0f;
    if ((ev.key.keysym.sym == SDLK_DOWN || ev.key.keysym.sym == SDLK_s) &&
        m_dollySpeed < 0)
      m_dollySpeed = 0.0f;
    if ((ev.key.keysym.sym == SDLK_LEFT || ev.key.keysym.sym == SDLK_a) &&
        m_panSpeed < 0)
      m_panSpeed = 0.0f;
    if ((ev.key.keysym.sym == SDLK_RIGHT || ev.key.keysym.sym == SDLK_d) &&
        m_panSpeed > 0)
      m_panSpeed = 0.0f;
    if (ev.key.keysym.sym == SDLK_q && m_truckSpeed < 0) m_truckSpeed = 0.0f;
    if (ev.key.keysym.sym == SDLK_e && m_truckSpeed > 0) m_truckSpeed = 0.0f;
  }
}

void OpenGLWindow::initializeGL() {
  abcg::glClearColor(0, 0, 0.1, 1);

  // Enable depth buffering
  abcg::glEnable(GL_DEPTH_TEST);

  // Create program
  m_program = createProgramFromFile(getAssetsPath() + "lookat.vert",
                                    getAssetsPath() + "lookat.frag");

  m_ground.initializeGL(m_program);

  m_modelTree.loadObj(getAssetsPath() + "arvore.obj");
  m_modelHand.loadObj(getAssetsPath() + "hand.obj");
  m_modelWolf.loadObj(getAssetsPath() + "lobo.obj");
  m_modelTable.loadObj(getAssetsPath() + "mesa.obj");
  m_modelSkull.loadObj(getAssetsPath() + "skull.obj");
  m_modelPlant.loadObj(getAssetsPath() + "plantinha.obj");
  m_modelRoom.loadObj(getAssetsPath() + "Room.obj");

  m_modelTree.setupVAO(m_program);
  m_modelHand.setupVAO(m_program);
  m_modelWolf.setupVAO(m_program);
  m_modelTable.setupVAO(m_program);
  m_modelSkull.setupVAO(m_program);
  m_modelPlant.setupVAO(m_program);
  m_modelRoom.setupVAO(m_program);

  resizeGL(getWindowSettings().width, getWindowSettings().height);
}

void OpenGLWindow::paintGL() {
  update();

  // Clear color buffer and depth buffer
  abcg::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  abcg::glViewport(0, 0, m_viewportWidth, m_viewportHeight);

  abcg::glUseProgram(m_program);

  // Get location of uniform variables (could be precomputed)
  const GLint viewMatrixLoc{
      abcg::glGetUniformLocation(m_program, "viewMatrix")};
  const GLint projMatrixLoc{
      abcg::glGetUniformLocation(m_program, "projMatrix")};
  const GLint modelMatrixLoc{
      abcg::glGetUniformLocation(m_program, "modelMatrix")};
  const GLint colorLoc{abcg::glGetUniformLocation(m_program, "color")};

  // Set uniform variables for viewMatrix and projMatrix
  // These matrices are used for every scene object
  abcg::glUniformMatrix4fv(viewMatrixLoc, 1, GL_FALSE,
                           &m_camera.m_viewMatrix[0][0]);
  abcg::glUniformMatrix4fv(projMatrixLoc, 1, GL_FALSE,
                           &m_camera.m_projMatrix[0][0]);

  // Arvore
  glm::mat4 model{1.0f};
  model = glm::translate(model, glm::vec3(-1.0f, 1.0f, 0.9f));
  model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
  model = glm::scale(model, glm::vec3(0.9f));
  abcg::glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE, &model[0][0]);
  abcg::glUniform4f(colorLoc, 1.0f, 5.0f, 1.0f, 1.0f);
  m_modelTree.render();

  // Cranio
  model = glm::mat4(1.0);
  model = glm::translate(model, glm::vec3(1.5f, 0.7f, 1.4f));  
  model = glm::scale(model, glm::vec3(0.4f));
  model = glm::rotate_slow(model, glm::radians(90.0f), glm::vec3(0, 0, 1)); //olha pra cima se por 180 gira horizontalmente
  model = glm::rotate_slow(model, glm::radians(90.0f), glm::vec3(0, 1, 0)); //deitou pro lado, gira queixo pra cima baixo
  //model = glm::rotate_slow(model, glm::radians(90.0f), glm::vec3(1, 0, 0)); //olhou pra baixo
  abcg::glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE, &model[0][0]);
  abcg::glUniform4f(colorLoc, 5.5f, 0.35f, 0.05f, 0.02f);
  m_modelSkull.render(); 

  // Mao
  model = glm::mat4(1.0);
  model = glm::translate(model, glm::vec3(0.2f, 0.7f, 1.6f));  
  model = glm::scale(model, glm::vec3(0.5f));
  model = glm::rotate(model, glm::radians(270.0f), glm::vec3(0.0f, 1.0f, 0.0f)); // where x, y, z is axis of rotation (e.g. 0 1 0)
  abcg::glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE, &model[0][0]);
  abcg::glUniform4f(colorLoc, 0.5f, 0.35f, 5.05f, 1.0f);
  m_modelHand.render();  

  // Lobo
  model = glm::mat4(1.0);
  model = glm::translate(model, glm::vec3(-1.2f, 0.5f, 1.6f));  
  model = glm::scale(model, glm::vec3(0.5f));
  model = glm::rotate(model, glm::radians(270.0f), glm::vec3(0.0f, 1.0f, 0.0f)); // where x, y, z is axis of rotation (e.g. 0 1 0)
  abcg::glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE, &model[0][0]);
  abcg::glUniform4f(colorLoc, 0.0f, 3.0f, 4.0f, 1.0f);
  m_modelWolf.render();  

  // Mesa
  model = glm::mat4(1.0);
  model = glm::translate(model, glm::vec3(-1.0f, 0.3f, 2.5f));  
  model = glm::scale(model, glm::vec3(0.25f));
  model = glm::rotate(model, glm::radians(270.0f), glm::vec3(0.0f, 1.0f, 0.0f)); // where x, y, z is axis of rotation (e.g. 0 1 0)
  abcg::glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE, &model[0][0]);
  abcg::glUniform4f(colorLoc, 5.5f, 0.35f, 5.05f, 1.0f);
  m_modelTable.render();  

  // Plantinha
  model = glm::mat4(1.0);
  model = glm::translate(model, glm::vec3(-1.0f, 0.56f, 2.5f));  
  model = glm::scale(model, glm::vec3(0.2f));
  model = glm::rotate(model, glm::radians(270.0f), glm::vec3(0.0f, 1.0f, 0.0f)); // where x, y, z is axis of rotation (e.g. 0 1 0)
  abcg::glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE, &model[0][0]);
  abcg::glUniform4f(colorLoc, 5.0f, 5.05f, 0.0f, 1.0f);
  m_modelPlant.render();  


  // Quarto
  model = glm::mat4(1.0);
  model = glm::translate(model, glm::vec3(0.0f, 0.6f, 2.0f));  
  model = glm::scale(model, glm::vec3(4.2f));
  model = glm::rotate(model, glm::radians(270.0f), glm::vec3(0.0f, 1.0f, 0.0f)); // where x, y, z is axis of rotation (e.g. 0 1 0)
  abcg::glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE, &model[0][0]);
  abcg::glUniform4f(colorLoc, 0.5f, 0.35f, 0.05f, 1.0f);
  m_modelRoom.render();

  // Draw ground
  m_ground.paintGL();

  abcg::glUseProgram(0);
}

void OpenGLWindow::paintUI() { abcg::OpenGLWindow::paintUI(); }

void OpenGLWindow::resizeGL(int width, int height) {
  m_viewportWidth = width;
  m_viewportHeight = height;

  m_camera.computeProjectionMatrix(width, height);
}

void OpenGLWindow::terminateGL() {
  m_ground.terminateGL();
  m_modelTree.terminateGL();
  m_modelHand.terminateGL();
  m_modelWolf.terminateGL();
  m_modelTable.terminateGL();
  m_modelSkull.terminateGL();
  m_modelPlant.terminateGL();
  m_modelRoom.terminateGL();

  abcg::glDeleteProgram(m_program);
}

void OpenGLWindow::update() {
  const float deltaTime{static_cast<float>(getDeltaTime())};

  // Update LookAt camera
  m_camera.dolly(m_dollySpeed * deltaTime);
  m_camera.truck(m_truckSpeed * deltaTime);
  m_camera.pan(m_panSpeed * deltaTime);
}